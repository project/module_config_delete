CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Useful for specific testing situations. This module allows you to enter the
machine name of a module, the path to the modules config files, and see if any
active config exists. You may then delete this active config.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/module_config_delete

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/module_config_delete


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install the Module Config Delete module as you would normally install a
  contributed Drupal module.
  Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Configuration > Development > Module Config
   Delete for configuration.


MAINTAINERS
-----------

* Preston Schmidt - https://www.drupal.org/u/prestosaurus
